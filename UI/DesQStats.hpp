/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

// Local Headers
#include <desqui/MainWindow.hpp>

// From libCSys
#include <desq/SysInfo.hpp>
#include <desq/NetworkInfo.hpp>
#include <desq/StorageInfo.hpp>

namespace DesQUI {
    class FlexiLayout;
    class UsagePie;
    class UsageGauge;
}

class QTableView;
class ProcessModel;

namespace DesQ {
    namespace Stats {
        class UI;
    }
}

class DesQ::Stats::UI : public DesQUI::MainWindow {
    Q_OBJECT;

    public:
        UI();

    private:
        /* Variables */
        QStackedWidget *pages;

        void createUI();
        void setWindowProperties();

        void buildPages();
        void buildActions();

    /* Private variables and Slots */
    private:
        /* Handle the various actions emitted from DesQActionBar */
        void handleAction( QString path );

        /* Update the cpu, ram and process table */
        void updateCPU();
        void updateRAM();
        void updateDisk();
        void updateNet();

        /* Stacked layout to handle multiple pages */
        QStackedLayout *mainLyt;

        /* A generic scroll to handle page overflows */
        QScrollArea *base;

        /* Root device */
        StorageDevice rootDev;

        /* UsageGauge for CPU and Network usage */
        DesQUI::UsageGauge *cpuGauge, *netGauge;

        /* UsagePie for HDD, RAM */
        DesQUI::UsagePie *ramPie, *hddPie;

        /* Info objects and timer */
        DesQ::SystemInfo *mSysInfo;
        DesQ::NetworkInfo *mNetInfo;
        QBasicTimer *mTimer;
        QBasicTimer *hddTimer;          // Update the HDD usage once every 10 mins
        QBasicTimer *procTimer;         // Update the HDDprocess table every 5s

        /* Page0 Layout */
        DesQUI::FlexiLayout *page0WidgetsLyt;
        QWidget *infoFrame;
        QLabel *uptimeLbl;
        QLabel *procsLbl;

        /* Process table */
        QTableView *procTable;
        ProcessModel *procModel;

        /* Storage for network info */
        qint64 prevRXBytes = 0, prevTXBytes = 0;

    /* Protected members */
    protected:
        void timerEvent( QTimerEvent *tEvent );
        void resizeEvent( QResizeEvent *rEvent );
};
