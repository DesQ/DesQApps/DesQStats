/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

// Local Headers
#include "DesQStats.hpp"
#include "ProcessModel.hpp"

// DesQ Headers
#include <desq/desq-config.h>
#include <desqui/FlexiLayout.hpp>
#include <desqui/UsagePie.hpp>
#include <desqui/UsageGauge.hpp>
#include <desqui/UsageGraph.hpp>

// DesQ Headers
#include <desq/SysInfo.hpp>
#include <desq/Utils.hpp>

inline static qint64 maxYForSize( qint64 fileSize ) {
    QList<qint64> sizes  = { 1ULL << 0, 1ULL << 10, 1ULL << 20, 1ULL << 30, 1ULL << 40 };
    QList<qint64> widths = { 1, 2, 5, 10, 20, 50, 100, 200, 500 };

    Q_FOREACH ( qint64 size, sizes ) {
        Q_FOREACH ( qint64 width, widths ) {
            if ( fileSize < width * size ) {
                return width * size;
            }
        }
    }

    return 0;
}


DesQ::Stats::UI::UI() : DesQUI::MainWindow( "Stats", nullptr ) {
    StorageManager *mgr = StorageManager::instance();

    for ( StorageBlock block: mgr->blocks() ) {
        if ( block.isValid() == false ) {
            continue;
        }

        if ( block.mountPoint() == "/" ) {
            rootDev = StorageDevice( DesQ::Utils::baseName( block.drive() ) );
            break;
        }
    }

    createUI();
    setWindowProperties();

    /* HDD info updates very slowly, so update now */
    updateDisk();
}


void DesQ::Stats::UI::createUI() {
    base = new QScrollArea( this );
    base->setFrameStyle( QFrame::NoFrame );
    base->setWidgetResizable( true );
    setMainView( base );

    QWidget *widget = new QWidget( this );
    mainLyt = new QStackedLayout();
    widget->setLayout( mainLyt );

    base->setWidget( widget );

    /* Build the pages */
    buildPages();

    /* Build the ActionBar */
    buildActions();

    /* Start the info update timer */
    mTimer->start( 1000, this );

    /* Start the process model update timer */
    procTimer->start( 5000, Qt::VeryCoarseTimer, this );

    /* HDD timer needs firing once every 10 mins or so only */
    hddTimer->start( 600000, Qt::VeryCoarseTimer, this );
}


void DesQ::Stats::UI::buildPages() {
    /* Info workers */
    mSysInfo = DesQ::SystemInfo::sysInfoObject();
    mNetInfo = DesQ::NetworkInfo::instance();

    /* Page 0: General ==== */

    infoFrame = new QWidget( this );
    infoFrame->setFixedWidth( 630 );
    infoFrame->setObjectName( "base" );
    infoFrame->setStyleSheet( "#base{ border: 1px solid palette(Text); border-radius: 5px; }" );

    QLabel *desqIcon = new QLabel();
    desqIcon->setPixmap( QIcon::fromTheme( "desq" ).pixmap( 128 ) );
    desqIcon->setFixedSize( QSize( 192, 192 ) );
    desqIcon->setAlignment( Qt::AlignCenter );
    desqIcon->setStyleSheet( "border: 5px solid palette(Highlight); border-radius: 96px;" );

    QLabel *desqText = new QLabel();
    desqText->setText(
        "<p><font size=30>DesQ </font>"
        "<font size=20>v" DESQ_VERSION_STR "</font></p>"
        "<big><big><big><big><big><big>https://desq-project.org"
    );
    desqText->setAlignment( Qt::AlignCenter );

    uptimeLbl = new QLabel( QString( "<b>Uptime</b> %1" ).arg( mSysInfo->uptime() ) );
    uptimeLbl->setFixedWidth( 360 );
    procsLbl = new QLabel( QString( "<b>Processes</b> %1 running (%2 threads)" ).arg( mSysInfo->getProcInfo().at( 0 ) ).arg( mSysInfo->getProcInfo().at( 1 ) ) );
    procsLbl->setFixedWidth( 360 );

    QVBoxLayout *infoLyt = new QVBoxLayout();
    infoLyt->addWidget( new QLabel( QString( "<b>%1</b>" ).arg( mSysInfo->product() ) ) );
    infoLyt->addWidget( new QLabel( QString( "<b>Processor</b> %1" ).arg( mSysInfo->cpuModel() ) ) );
    infoLyt->addWidget( new QLabel( QString( "<b>Memory</b> %1" ).arg( DesQ::Utils::formatSize( mSysInfo->getRamUsage().at( 1 ) ) ) ) );
    infoLyt->addWidget( new QLabel( QString( "<hr>" ) ) );
    infoLyt->addWidget( new QLabel( QString( "<b>Distribution</b> %1" ).arg( mSysInfo->distribution() ) ) );
    infoLyt->addWidget( new QLabel( QString( "<b>Kernel</b> %1" ).arg( mSysInfo->kernelVersion() ) ) );
    infoLyt->addWidget( new QLabel( QString( "<b>Qt Version</b> %1" ).arg( QT_VERSION_STR ) ) );
    infoLyt->addWidget( new QLabel( QString( "<hr>" ) ) );
    infoLyt->addWidget( uptimeLbl );
    infoLyt->addWidget( procsLbl );

    QGridLayout *frameLyt = new QGridLayout();
    frameLyt->setContentsMargins( QMargins( 20, 20, 20, 20 ) );
    frameLyt->setSpacing( 10 );
    frameLyt->addWidget( desqIcon, 0, 0, 2, 1, Qt::AlignCenter );
    frameLyt->addWidget( desqText, 0, 1, 1, 1, Qt::AlignCenter );
    frameLyt->addLayout( infoLyt, 1, 1, 1, 1, Qt::AlignCenter );

    infoFrame->setLayout( frameLyt );

    /* Timers */
    mTimer    = new QBasicTimer();
    hddTimer  = new QBasicTimer();
    procTimer = new QBasicTimer();

    cpuGauge = new DesQUI::UsageGauge( "CPU", 4, this );
    cpuGauge->setMaximum( 1024 );
    cpuGauge->setUnits( DesQUI::UsageGauge::PERCENT );
    cpuGauge->setMinimumRadius( 75 );

    ramPie = new DesQUI::UsagePie( "RAM", 2, this );
    ramPie->setUnits( DesQUI::UsagePie::SIZE );
    ramPie->setMinimumRadius( 75 );

    hddPie = new DesQUI::UsagePie( "HDD", rootDev.validPartitions().count(), this );
    hddPie->setUnits( DesQUI::UsagePie::PERCENT );
    hddPie->setLabels( { "sda1", "sda2", "sda5", "sda6" } );
    hddPie->setMinimumRadius( 75 );

    netGauge = new DesQUI::UsageGauge( "Net", 2, this );
    netGauge->setUnits( DesQUI::UsageGauge::SPEED );
    netGauge->setMaximum( 100 );
    netGauge->setMinimumRadius( 75 );

    page0WidgetsLyt = new DesQUI::FlexiLayout();
    page0WidgetsLyt->setAlignment( Qt::AlignJustify );
    page0WidgetsLyt->addWidget( cpuGauge );
    page0WidgetsLyt->addWidget( ramPie );
    page0WidgetsLyt->addWidget( hddPie );
    page0WidgetsLyt->addWidget( netGauge );

    QWidget     *page0    = new QWidget();
    QVBoxLayout *page0Lyt = new QVBoxLayout();
    page0Lyt->setAlignment( Qt::AlignCenter );

    page0Lyt->addWidget( infoFrame, 0, Qt::AlignCenter );
    page0Lyt->addLayout( page0WidgetsLyt, 1 );
    page0Lyt->addStretch();

    page0->setLayout( page0Lyt );

    /* Page1 */
    procTable = new QTableView( this );
    procTable->setFrameStyle( QFrame::NoFrame );
    procTable->horizontalHeader()->setStretchLastSection( true );
    procTable->verticalHeader()->hide();
    procTable->setGridStyle( Qt::NoPen );
    procTable->setSortingEnabled( true );

    procModel = new ProcessModel( procTable );

    QSortFilterProxyModel *sortProxyModel = new QSortFilterProxyModel( this );
    sortProxyModel->setSourceModel( procModel );
    sortProxyModel->setSortRole( Qt::UserRole + 1 );
    sortProxyModel->sort( 2, Qt::DescendingOrder );

    procTable->setModel( sortProxyModel );

    mainLyt->addWidget( page0 );
    mainLyt->addWidget( procTable );
}


void DesQ::Stats::UI::buildActions() {
    QFileSystemModel *fsm = new QFileSystemModel();

    fsm->setRootPath( "/home/cosmos/" );

    QListView *widget3 = new QListView( this );
    widget3->setFrameStyle( QFrame::NoFrame );
    widget3->setModel( fsm );
    widget3->setRootIndex( fsm->index( "/home/cosmos/" ) );

    QTableView *widget4 = new QTableView( this );
    widget4->setFrameStyle( QFrame::NoFrame );
    widget4->setModel( fsm );
    widget4->setRootIndex( fsm->index( "/home/cosmos/" ) );

    mainLyt->addWidget( widget3 );
    mainLyt->addWidget( widget4 );

    /* Page 0 */
    addAction( 0, "General",   QIcon( ":/icons/desq-stats.png" ),        "page0",    "General",        true, true );
    addAction( 0, "Processes", QIcon( ":/icons/processes.png" ),         "page1;#1", "Show Processes", true, false );
    addAction( 0, "Drives",    QIcon::fromTheme( "drive-harddisk" ),     "page2;#2", "Show Resources", true, false );
    addAction( 0, "Terminal",  QIcon::fromTheme( "utilities-terminal" ), "page3;#3", "Show Resources", true, false );

    /* Page 1 */
    addAction( 1, "General",   QIcon( ":/icons/desq-stats.png" ),        "page0;#0", "Show Resources", true, false );
    addAction( 1, "Processes", QIcon( ":/icons/processes.png" ),         "page1;",   "Show Processes", true, true );
    addAction( 1, "Drives",    QIcon::fromTheme( "drive-harddisk" ),     "page2;#2", "Show Resources", true, false );
    addAction( 1, "Terminal",  QIcon::fromTheme( "utilities-terminal" ), "page3;#3", "Show Resources", true, false );

    /* Page 2 */
    addAction( 2, "General",   QIcon( ":/icons/desq-stats.png" ),        "page0;#0", "Show Resources", true, false );
    addAction( 2, "Processes", QIcon( ":/icons/processes.png" ),         "page1;#1", "Show Processes", true, false );
    addAction( 2, "Drives",    QIcon::fromTheme( "drive-harddisk" ),     "page2;",   "Show Resources", true, true );
    addAction( 2, "Terminal",  QIcon::fromTheme( "utilities-terminal" ), "page3;#3", "Show Resources", true, false );

    /* Page 3 */
    addAction( 3, "General",   QIcon( ":/icons/desq-stats.png" ),        "page0;#0", "Show Resources", true, false );
    addAction( 3, "Processes", QIcon( ":/icons/processes.png" ),         "page1;#1", "Show Processes", true, false );
    addAction( 3, "Drives",    QIcon::fromTheme( "drive-harddisk" ),     "page2;#2", "Show Resources", true, false );
    addAction( 3, "Terminal",  QIcon::fromTheme( "utilities-terminal" ), "page3;",   "Show Resources", true, true );

    connect( this, &DesQUI::MainWindow::actionRequested, this, &DesQ::Stats::UI::handleAction );
}


void DesQ::Stats::UI::setWindowProperties() {
    setAppTitle( "DesQ Stats" );
    setAppIcon( QIcon( ":/icons/desq-stats.png" ) );

    setWindowFlags( Qt::FramelessWindowHint );
    resize( 800, 600 );

    setMinimumWidth( 300 );
}


void DesQ::Stats::UI::handleAction( QString action ) {
    if ( action == "page1" ) {
        mainLyt->setCurrentIndex( 1 );
    }

    else if ( action == "page2" ) {
        mainLyt->setCurrentIndex( 2 );
    }

    else if ( action == "page3" ) {
        mainLyt->setCurrentIndex( 3 );
    }

    else {
        mainLyt->setCurrentIndex( 0 );
    }
}


void DesQ::Stats::UI::updateCPU() {
    GaugeResourceValues cpuValues;

    cpuValues << mSysInfo->getCpuUsage();
    cpuValues.removeFirst();            // First one returned by mSysInfo is the average

    QStringList labels;
    for ( qint64 value: cpuValues ) {
        labels << QString( "%1 %" ).arg( value );
    }

    cpuGauge->setValues( cpuValues );
    cpuGauge->setLabels( labels );
}


void DesQ::Stats::UI::updateRAM() {
    QList<qint64> ramUsage = mSysInfo->getRamUsage();
    QList<qint64> swpUsage = mSysInfo->getSwapUsage();

    ramPie->setValues( { { ramUsage[ 1 ], ramUsage[ 0 ] }, { swpUsage[ 1 ], swpUsage[ 0 ] } } );
    ramPie->setLabels( { DesQ::Utils::formatSize( ramUsage[ 0 ] ), DesQ::Utils::formatSize( swpUsage[ 0 ] ) } );
}


void DesQ::Stats::UI::updateDisk() {
    PieResourceValues values;
    QStringList       labels;

    Q_FOREACH ( StorageBlock block, rootDev.validPartitions() ) {
        values << PieResourceValue{ block.totalSize(), (block.totalSize() - block.availableSize() ) };
        labels << QString( "%1 %" ).arg( 100 * (block.totalSize() - block.availableSize() ) / block.totalSize() );
    }

    hddPie->setValues( values );
    hddPie->setLabels( labels );
}


void DesQ::Stats::UI::updateNet() {
    QList<qint64> IOBytes = mNetInfo->getIOBytes();

    qint64 RecvSpeed = prevRXBytes ? IOBytes[ 0 ] - prevRXBytes : 0;
    qint64 TrnsSpeed = prevTXBytes ? IOBytes[ 1 ] - prevTXBytes : 0;

    netGauge->setValues( { RecvSpeed, TrnsSpeed } );
    netGauge->setMaximum( maxYForSize( std::max( { RecvSpeed, TrnsSpeed } ) ) );

    QStringList labels;
    labels << QString( "\u2193 %1" ).arg( DesQ::Utils::formatSizeRaw( RecvSpeed ), 0, 'f', 1 );
    labels << QString( "\u2191 %1" ).arg( DesQ::Utils::formatSizeRaw( TrnsSpeed ), 0, 'f', 1 );
    netGauge->setLabels( labels );

    prevRXBytes = IOBytes[ 0 ];
    prevTXBytes = IOBytes[ 1 ];
}


void DesQ::Stats::UI::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == mTimer->timerId() ) {
        updateCPU();
        updateRAM();
        updateNet();

        uptimeLbl->setText( QString( "<b>Uptime</b> %1" ).arg( mSysInfo->uptime() ) );
    }

    else if ( tEvent->timerId() == hddTimer->timerId() ) {
        updateDisk();
    }

    else if ( tEvent->timerId() == procTimer->timerId() ) {
        /* Update the process table */
        procModel->reload();
        procsLbl->setText( QString( "<b>Processes</b> %1 running (%2 threads)" ).arg( mSysInfo->getProcInfo().at( 0 ) ).arg( mSysInfo->getProcInfo().at( 1 ) ) );
    }

    else {
        DesQUI::MainWindow::timerEvent( tEvent );
    }
}


void DesQ::Stats::UI::resizeEvent( QResizeEvent *rEvent ) {
    DesQUI::MainWindow::resizeEvent( rEvent );

    if ( mainLyt->currentIndex() == 1 ) {
        procTable->resize( base->viewport()->size() );
    }

    qDebug() << page0WidgetsLyt->geometry();

    QRect progLytRect( page0WidgetsLyt->geometry() );
    progLytRect.setSize( base->viewport()->size() );
    page0WidgetsLyt->setGeometry( progLytRect );
}
