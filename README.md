# DesQ Stats
## A System Stats view for DesQ DE


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQStats.git DesQStats`
- Enter the `DesQStats` folder
  * `cd DesQStats`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`

### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)

## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.1

### Upcoming
* Any other feature you request for... :)
