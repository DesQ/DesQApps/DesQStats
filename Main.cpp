/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include <signal.h>

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "Global.hpp"
#include "DesQStats.hpp"

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFApplication.hpp>

#include <desq/Utils.hpp>

DFL::Settings *statsSett = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Stats.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Stats started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app( argc, argv );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Stats" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-stats" );

    app.interceptSignal( SIGINT,  true );
    app.interceptSignal( SIGTERM, true );
    app.interceptSignal( SIGQUIT, true );
    app.interceptSignal( SIGABRT, true );
    app.interceptSignal( SIGSEGV, true );

    if ( app.lockApplication() ) {
        statsSett = DesQ::Utils::initializeDesQSettings( "Stats", "Stats" );

        DesQ::Stats::UI *stats = new DesQ::Stats::UI();
        // QObject::connect( &app, &DFL::Application::messageFromClient, stats,
        // &DesQ::Stats::UI::receiveMessage );
        stats->show();
    }

    return app.exec();
}
