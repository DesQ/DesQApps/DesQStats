/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "ProcessModel.hpp"
#include <sys/sysinfo.h>
#include <unistd.h>
#include <desq/Utils.hpp>

static int PAGE_SIZE = sysconf( _SC_PAGESIZE );
double     ProcessModel::RAM_SIZE = -1.0;

ProcessModel::ProcessModel( QObject *parent ) : QAbstractTableModel( parent ) {
    if ( RAM_SIZE <= 0 ) {
        struct sysinfo info;

        if ( sysinfo( &info ) ) {
            RAM_SIZE = -1;
        }

        RAM_SIZE = info.totalram * info.mem_unit;
    }

    reload();
}


int ProcessModel::rowCount( const QModelIndex& ) const {
    return processMap.keys().count();
}


int ProcessModel::columnCount( const QModelIndex& ) const {
    return 5;
}


QVariant ProcessModel::data( const QModelIndex& index, int role ) const {
    if ( index.row() >= processMap.keys().count() ) {
        return QVariant();
    }

    switch ( role ) {
        case Qt::DisplayRole: {
            switch ( index.column() ) {
                case 0: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 0 );       // PID
                }

                case 1: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 1 );       // User
                }

                case 2: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 2 );       // CPU
                                                                                                        // %
                                                                                                        // 3
                                                                                                        // will
                                                                                                        // contain
                                                                                                        // double
                                                                                                        // value
                }

                case 3: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 4 );       // RAM
                                                                                                        // %
                                                                                                        // 5
                                                                                                        // will
                                                                                                        // contain
                                                                                                        // double
                                                                                                        // value
                }

                case 4: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 6 );       // Cmd
                }

                default: {
                    return QVariant();
                }
            }
        }

        case Qt::TextAlignmentRole: {
            switch ( index.column() ) {
                case 0: {
                    return Qt::AlignCenter;
                }

                case 1: {
                    return Qt::AlignCenter;
                }

                case 2: {
                    return Qt::AlignCenter;
                }

                case 3: {
                    return Qt::AlignCenter;
                }

                case 4: {
                    return ( int )Qt::AlignVCenter | Qt::AlignLeft;
                }

                default: {
                    return QVariant();
                }
            }
        }

        case Qt::UserRole + 1: {
            switch ( index.column() ) {
                case 0: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 0 );       // PID
                }

                case 1: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 1 );       // User
                }

                case 2: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 3 );       // CPU
                                                                                                        // %
                                                                                                        // 3
                                                                                                        // will
                                                                                                        // contain
                                                                                                        // double
                                                                                                        // value
                }

                case 3: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 5 );       // RAM
                                                                                                        // %
                                                                                                        // 5
                                                                                                        // will
                                                                                                        // contain
                                                                                                        // double
                                                                                                        // value
                }

                case 4: {
                    return processMap.value( processMap.keys().value( index.row() ) ).value( 6 );       // Cmd
                }

                default: {
                    return QVariant();
                }
            }
        }

        default: {
            return QVariant();
        }
    }

    return QVariant();
}


QVariant ProcessModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( orientation != Qt::Horizontal ) {
        return QVariant();
    }

    switch ( role ) {
        case Qt::DisplayRole: {
            switch ( section ) {
                case 0: {
                    return "PID";
                }

                case 1: {
                    return "User";
                }

                case 2: {
                    return "CPU %";
                }

                case 3: {
                    return "Mem %";
                }

                case 4: {
                    return "Command line";
                }

                default: {
                    return QVariant();
                }
            }
        }

        case Qt::ToolTipRole: {
            switch ( section ) {
                case 0: {
                    return "Process ID";
                }

                case 1: {
                    return "User who invoked this process";
                }

                case 2: {
                    return "Percentage of CPU used";
                }

                case 3: {
                    return "Percentage of RAM used";
                }

                case 4: {
                    return "Command line which started this process";
                }

                default: {
                    return QVariant();
                }
            }
        }

        default: {
            return QVariant();
        }
    }

    return QVariant();
}


void ProcessModel::reload() {
    beginResetModel();

    /* Get the current CPU Time */
    quint64 user = 0, nice = 0, system = 0, idle = 0;

    QFile proc_stat( "/proc/stat" );
    proc_stat.open( QFile::ReadOnly );
    QString line = QString::fromUtf8( proc_stat.readLine() );
    proc_stat.close();
    sscanf( line.toUtf8().constData(), "%*s %llu %llu %llu %llu", &user, &nice, &system, &idle );
    curCpuTime = user + nice + system + idle;

    /* Start obtaining process info */
    processMap.clear();

    QDir procDir( "/proc" );
    procDir.setFilter( QDir::Dirs );

    QMap<QString, QStringList> procs;

    bool isNumeric = false;
    Q_FOREACH ( QFileInfo dir, procDir.entryInfoList() ) {
        int pid = dir.fileName().toInt( &isNumeric );

        if ( isNumeric ) {
            if ( dir.owner() != QFileInfo( QDir::homePath() ).owner() ) {
                continue;
            }

            processMap[ pid ] = QVariantList();
            processMap[ pid ] << pid;                           // Process ID
            processMap[ pid ] << dir.owner();                   // User name
            getCPUInfo( pid );                                  // CPU usage info
            getRAMInfo( pid );                                  // RAM usage info

            QFile cli( dir.filePath() + "/cmdline" );

            if ( cli.open( QFile::ReadOnly ) ) {
                processMap[ pid ] << QString::fromLocal8Bit( cli.readAll().split( '\x00' ).join( " " ) );
            }

            else {
                processMap[ pid ] << QString( "unknown" );
            }

            cli.close();
        }
    }

    /* Update the lastCpuTime */
    lastCpuTime = curCpuTime;

    endResetModel();
}


void ProcessModel::getCPUInfo( int pid ) {
    /*
     *
     * processCpuMap will store two values per pid.
     *    First will be cutime + cstime from /proc/<pid>/stat (fields 15, 16)
     *    Second will be total of all fields from /proc/stat
     *
     */

    quint64 cutime_cstime = 0;

    QFile stat( QString( "/proc/%1/stat" ).arg( pid ) );

    if ( not stat.open( QFile::ReadOnly ) ) {
        processMap[ pid ] << 0;
        return;
    }

#if QT_VERSION >= 0x050E01
    QStringList data = QString( stat.readAll() ).split( " ", Qt::SkipEmptyParts );
#else
    QStringList data = QString( stat.readAll() ).split( " ", QString::SkipEmptyParts );
#endif
    cutime_cstime = data.value( 13 ).toULongLong() + data.value( 14 ).toULongLong();

    quint64 oldtimes = processCpuMap.value( pid );

    /* Update the model data */
    processMap[ pid ] << QString::number( 100.0 * (cutime_cstime - oldtimes) / (curCpuTime - lastCpuTime), 'f', 2 );
    processMap[ pid ] << 100.0 * (cutime_cstime - oldtimes) / (curCpuTime - lastCpuTime);

    /* Update the last usage info */
    processCpuMap[ pid ] = cutime_cstime;
}


void ProcessModel::getRAMInfo( int pid ) {
    QFile statm( QString( "/proc/%1/statm" ).arg( pid ) );

    if ( not statm.open( QFile::ReadOnly ) ) {
        processMap[ pid ] << 0;
        return;
    }

#if QT_VERSION >= 0x050E01
    QStringList data = QString( statm.readAll() ).split( " ", Qt::SkipEmptyParts );
#else
    QStringList data = QString( statm.readAll() ).split( " ", QString::SkipEmptyParts );
#endif
    processMap[ pid ] << QString::number( data.value( 1 ).toDouble() * PAGE_SIZE / RAM_SIZE * 100.0, 'f', 2 );
    processMap[ pid ] << data.value( 1 ).toDouble() * PAGE_SIZE / RAM_SIZE * 100.0;
}
